//
//  MovieTableViewCell.swift
//  InChurchFilmes
//
//  Created by Junior Fernandes on 03/02/21.
//

import UIKit
import Kingfisher
import SkeletonView


class MovieTableViewCell: UITableViewCell {
    @IBOutlet weak var ivMovie: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblVoteAverage: UILabel!
    @IBOutlet weak var lblReleaseDate: UILabel!
    @IBOutlet weak var lblTrailer: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupSkeleton()
    }

    func setupCell(movie: Result?) {
        guard let movie = movie else {
            return
        }
        let urlImage = Constants.urlImge + movie.posterPath
        self.ivMovie.kf.setImage(with: URL(string: urlImage))
        self.lblTitle.text = movie.title
        self.lblContent.text = movie.overview
        self.lblVoteAverage.text = String(movie.voteAverage)
        self.lblReleaseDate.text = movie.releaseDate
    }

    func setupCellFavorite(movie: FavoriteMovie?) {
        guard let movie = movie else {
            return
        }
        let urlImage = Constants.urlImge + movie.photo
        self.ivMovie.kf.setImage(with: URL(string: urlImage))
        self.lblTitle.text = movie.title
        self.lblContent.text = movie.overView
    }

    func setupSkeleton() {
        ivMovie.isSkeletonable = true
        lblTitle.isSkeletonable = true
        lblContent.isSkeletonable = true
    }
    func startAnimation() {
        self.ivMovie.showAnimatedGradientSkeleton()
        self.lblTitle.showAnimatedGradientSkeleton()
        self.lblContent.showAnimatedGradientSkeleton()
    }

    func stopAnimation() {
        self.ivMovie.hideSkeleton()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
