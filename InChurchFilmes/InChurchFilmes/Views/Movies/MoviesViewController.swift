//
//  MoviesViewController.swift
//  InChurchFilmes
//
//  Created by Junior Fernandes on 03/02/21.
//

import UIKit
import SkeletonView

class MoviesViewController: UIViewController {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var movies: [Result] = []
    
    var page: Int = 1

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.getMovie()
//        start loading
    }

    //MARK: - Methods

    private func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "MovieTableViewCell", bundle: nil), forCellReuseIdentifier: "MovieTableViewCell")
    }

    func getMovie() {
        let rota = Constants.baseURL + EndPoint.movie.rawValue + Constants.apiKey + "&page=\(page)"
        print(rota)

        self.startLoading()
        Network.shared.getPopularMovies(rota: rota, page: self.page) { (data, error) in
            print(data?.results)
            guard error == nil else{
                print(error?.localizedDescription)
                
                return
            }

            self.movies += data!.results
            self.page += 1
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.stopLoading()
            }
        }
    }

    func startLoading() {
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
    }

    func stopLoading() {
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
    }

}

extension MoviesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count ?? 5
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieTableViewCell", for: indexPath) as! MovieTableViewCell
//        guard let movie = movies[indexPath.row] else {
//            cell.setupCell(movie: nil)
//            cell.startAnimation()
//            return cell
//        }


        cell.setupCell(movie: self.movies[indexPath.row])
        cell.stopAnimation()
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if indexPath.row == self.movies.count - 10 {
            self.getMovie()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("CLIQUEI")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("CLIQUEI")
//        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//            if segue.identifier == "movieDetail" {
        let vc = segue.destination as? MoviewDetailViewController
//        let movie = movies[tableView.indexPathForSelectedRow!.row]
//        vc?.movieAll = movie
//            }
//        }
    }
}
