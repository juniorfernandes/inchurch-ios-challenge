//
//  FavoritesViewController.swift
//  InChurchFilmes
//
//  Created by Junior Fernandes on 05/02/21.
//

import UIKit

class FavoritesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    let cellID = "MovieTableViewCell"

    var favoriteMovies: [FavoriteMovie] = []
    var searchMovie: [FavoriteMovie] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.favoriteMovies = RealmService.shared.getFavorite()
        self.searchMovie = self.favoriteMovies
        self.searchBar.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {

        DispatchQueue.main.async {
            self.favoriteMovies = RealmService.shared.getFavorite()
            self.tableView.reloadData()
        }
        print("Aqui")
    }

    func setupTableView() {
        self.tableView.tableFooterView = UIView()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: cellID, bundle: nil), forCellReuseIdentifier: cellID)
    }
}

extension FavoritesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchMovie.count > 0 {
            return self.searchMovie.count
        } else {
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.searchMovie.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! MovieTableViewCell
            cell.setupCellFavorite(movie: self.searchMovie[indexPath.row])
            return cell
        } else {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            cell.textLabel?.text = "sem resultado"
            return cell
        }
    }
}

extension FavoritesViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchMovie = []
        if searchText.isEmpty {
            self.searchMovie = self.favoriteMovies
        } else {
            for movie in favoriteMovies {
                if movie.title.lowercased().contains(searchText.lowercased()) {
                    searchMovie.append(movie)
                }
            }
        }
        self.tableView.reloadData()
    }
}
