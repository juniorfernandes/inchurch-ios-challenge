//
//  MovieHomeTableViewCell.swift
//  InChurchFilmes
//
//  Created by Junior Fernandes on 03/02/21.
//

import UIKit
import Kingfisher

protocol MovieHomeTableViewCellDelegate {
    func favorite(movie: ResultTopRate?)
}

class MovieHomeTableViewCell: UICollectionViewCell {
    @IBOutlet weak var ivMovie: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblReleaseDate: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!

    var movie: ResultTopRate?
    var delegate: MovieHomeTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.favoriteButton.tintColor = UIColor(red: 212/255, green: 175/255, blue: 55/255, alpha: 1)
        self.setupView()
    }

    func setupCell(movie: ResultTopRate?, favorite: Bool) {
        self.movie = movie

        guard let movie = movie else {
            return
        }
        self.favoriteButton.setImage(UIImage(systemName: "star"), for: .normal)

        if favorite {
            self.favoriteButton.setImage(UIImage(systemName: "star.fill"), for: .normal)
        }

        let urlImage = Constants.urlImge + movie.posterPath
        self.ivMovie.kf.setImage(with: URL(string: urlImage), placeholder: UIImage(systemName: "camera"), options: [.keepCurrentImageWhileLoading, .transition(ImageTransition.fade(0.5))], completionHandler: nil)
        self.lblTitle.text = movie.title
        self.lblReleaseDate.text = movie.releaseDate
    }

    private func setupView() {
        self.ivMovie.layer.cornerRadius = 8
    }
    
    @IBAction func actionFavorite(_ sender: UIButton) {
        self.delegate?.favorite(movie: self.movie)
    }
}
