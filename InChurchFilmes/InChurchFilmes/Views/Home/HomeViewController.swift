//
//  HomeViewController.swift
//  InChurchFilmes
//
//  Created by Junior Fernandes on 03/02/21.
//

import UIKit
import SkeletonView

class HomeViewController: UIViewController {
    @IBOutlet weak var cvPopularMovies: UICollectionView!
    @IBOutlet weak var cvTopRatedMovies: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnSeeAll: UIButton!

    var topRateMovies: [ResultTopRate] = []
    var popularMovies: [ResultTopRate] = []
    var favoriteMovies: [FavoriteMovie] = []

    var page = 1

    let cellID = "MovieHomeTableViewCell"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getTopRatedMovie()
        self.getPopularMovies()
        self.setupCollectionView()
        self.favoriteMovies = RealmService.shared.getFavorite()
        print(self.favoriteMovies)

    }

    private func setupCollectionView() {
        self.cvPopularMovies.delegate = self
        self.cvPopularMovies.dataSource = self
        self.cvTopRatedMovies.delegate = self
        self.cvTopRatedMovies.dataSource = self
        let nibCell = UINib(nibName: cellID, bundle: nil)
        cvTopRatedMovies.register(nibCell, forCellWithReuseIdentifier: cellID)

        let nibCell2 = UINib(nibName: cellID, bundle: nil)
        cvPopularMovies.register(nibCell2, forCellWithReuseIdentifier: cellID)
    }

    private func getPopularMovies() {
        let rota = Constants.baseURL + EndPoint.movie.rawValue + Constants.apiKey + "&page=\(page)"
        self.startLoading()
        Network.shared.getTopRatedMovies(rota: rota, page: self.page) { (data, error) in
            guard error == nil else{
                return
            }
            self.popularMovies.append(contentsOf: data!.results)
            DispatchQueue.main.async {
                self.cvPopularMovies.reloadData()
                self.stopLoading()
            }
        }
    }

    func getTopRatedMovie() {
        let rota = Constants.baseURL + EndPoint.topRated.rawValue + Constants.apiKey + "&page=\(page)"
        Network.shared.getTopRatedMovies(rota: rota, page: self.page) { (data, error) in
            guard error == nil else{
                return
            }
            self.topRateMovies.append(contentsOf: data!.results)
            DispatchQueue.main.async {
                self.cvTopRatedMovies.reloadData()
            }
        }
    }

    func startLoading() {
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
    }
    
    func stopLoading() {
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
    }


    @IBAction func actionSeeAll(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 1
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return topRateMovies.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == cvPopularMovies {
            let movie = self.popularMovies[indexPath.row]
            let favorites = favoriteMovies.filter { (favoriteMovie) -> Bool in
                return favoriteMovie.id == movie.id
            }

            let isFavorite = favorites.count == 0 ? false : true

            let cell2 = cvPopularMovies.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! MovieHomeTableViewCell
            cell2.setupCell(movie: self.popularMovies[indexPath.row], favorite: isFavorite)
            cell2.delegate = self
            return cell2
        } else {
            let cell = cvTopRatedMovies.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! MovieHomeTableViewCell
            cell.setupCell(movie: self.topRateMovies[indexPath.row], favorite: true)
            cell.delegate = self
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var movie: ResultTopRate?

        if collectionView == cvPopularMovies {
            movie = self.popularMovies[indexPath.row]
        } else {
            movie = self.topRateMovies[indexPath.row]
        }

        self.performSegue(withIdentifier: "movieDetail", sender: movie)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "movieDetail" {
            let vc = segue.destination as? MoviewDetailViewController
            vc?.movie = sender as? ResultTopRate
        }
    }
}

extension HomeViewController: MovieHomeTableViewCellDelegate {
    func favorite(movie: ResultTopRate?) {
        guard let movie = movie else {
            print("Erro ao favoritar filme")
            return
        }
        let movieFavorite = FavoriteMovie()
        movieFavorite.setup(id: movie.id, title: movie.title, photo: movie.posterPath, releaseDate: movie.releaseDate, overView: movie.overview)
        RealmService.shared.saveFavoriteMovie(movie: movieFavorite)
        self.cvPopularMovies.reloadData()
    }
}
