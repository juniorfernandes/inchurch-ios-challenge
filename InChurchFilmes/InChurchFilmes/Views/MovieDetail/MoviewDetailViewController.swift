//
//  MoviewDetailViewController.swift
//  InChurchFilmes
//
//  Created by Junior Fernandes on 08/02/21.
//

import UIKit
import Kingfisher

class MoviewDetailViewController: UIViewController {
    var movie: ResultTopRate?
    @IBOutlet weak var ivMovie: UIImageView!
    @IBOutlet weak var lblOverView: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDataRelease: UILabel!
    @IBOutlet weak var lblGender: UILabel!

    var genderDetail: [Int] = []
    var genders = [GenreModel]()
    var genresData = [GenreModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getGender()
        self.setupView()
        genderDetail = self.movie!.genreIDS
    }

    func setupView() {
        let image = Constants.urlImge + movie!.backdropPath
        self.ivMovie.kf.setImage(with: URL(string: image), placeholder: UIImage(systemName: "camera"), options: [.keepCurrentImageWhileLoading, .transition(ImageTransition.fade(0.5))], completionHandler: nil)
        self.lblOverView.text = self.movie?.overview
        self.lblTitle.text = self.movie?.title
        self.lblDataRelease.text = "Lançado em \(self.movie?.releaseDate)"
    }

    func getGender() {
//        guard let genders = self.movie?.genreIDS else { return }
        let rota = Constants.baseURL + EndPoint.genre.rawValue + Constants.apiKey
        print(rota)
        Network.shared.getGenrer(rota: rota) { (result, error) in
            guard error == nil else{
                return
            }
            
            self.genresData.append(contentsOf: result!.genres)
            for genrer in self.genderDetail {
                for a in self.genresData {
                    if genrer == a.id {
                        DispatchQueue.main.async {
                            self.genders.append(a)
                            self.lblGender.text! += a.name + "  "
                        }
                    }
                }
            }
        }
    }

    @IBAction func actionClose(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
