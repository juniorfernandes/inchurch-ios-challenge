//
//  RealmService.swift
//  InChurchFilmes
//
//  Created by Junior Fernandes on 05/02/21.
//

import Foundation
import RealmSwift

class RealmService {
    static let shared = RealmService()
    private let realm = try! Realm()

    func deleteAll() {
        try! self.realm.write {
            realm.deleteAll()
        }
    }
    func saveFavoriteMovie(movie: FavoriteMovie) {
        var haveMovie = search(title: movie.title)

        if haveMovie > 0 {
            print("Filme favoritado")
            return
        }

        try! self.realm.write {
            self.realm.add(movie)
        }
    }

    func search(title: String) -> Int {
        let predicate = NSPredicate(format: "title = %@", title)
        var a: Int = 0
        try! self.realm.write {
            var favoriteMovies = self.realm.objects(FavoriteMovie.self).filter(predicate)
            a = favoriteMovies.count
        }
        return a
    }

    func getFavorite() -> [FavoriteMovie] {
        try! self.realm.write {
            var favorites: [FavoriteMovie] = []
            var favoriteMovies = self.realm.objects(FavoriteMovie.self)
            for movie in favoriteMovies {
                let a = FavoriteMovie()
                a.setup(id: movie.id, title: movie.title, photo: movie.photo, releaseDate: movie.releaseDate, overView: movie.overView)
                favorites.append(a)
            }
            return favorites
        }
    }
}
