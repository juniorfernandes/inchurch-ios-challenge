//
//  Constants.swift
//  InChurchFilmes
//
//  Created by Junior Fernandes on 03/02/21.
//

import Foundation

struct Constants {
    static let apiKey = "api_key=5638b412d6b3434f17d48ebd63eed875"
    static let baseURL = "https://api.themoviedb.org/3/"
    static let urlImge = "https://image.tmdb.org/t/p/w500"
}
