//
//  GenreModel.swift
//  InChurchFilmes
//
//  Created by Junior Fernandes on 09/02/21.
//

import Foundation

struct ListGenre: Codable {
    let genres: [GenreModel]
}

struct GenreModel: Codable {
    let id: Int
    let name: String
}
