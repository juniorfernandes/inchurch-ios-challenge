//
//  TopRatedMoviesModel.swift
//  InChurchFilmes
//
//  Created by Junior Fernandes on 05/02/21.
//

import Foundation

//{
//      "adult": false,
//      "backdrop_path": "/fQq1FWp1rC89xDrRMuyFJdFUdMd.jpg",
//      "genre_ids": [
//        10749,
//        35
//      ],
//      "id": 761053,
//      "original_language": "en",
//      "original_title": "Gabriel's Inferno Part III",
//      "overview": "The final part of the film adaption of the erotic romance novel Gabriel's Inferno written by an anonymous Canadian author under the pen name Sylvain Reynard.",
//      "popularity": 36.755,
//      "poster_path": "/fYtHxTxlhzD4QWfEbrC1rypysSD.jpg",
//      "release_date": "2020-11-19",
//      "title": "Gabriel's Inferno Part III",
//      "video": false,
//      "vote_average": 9,
//      "vote_count": 659
//}

struct TopRatedMoviesModel: Codable {
    let page: Int
    let results: [ResultTopRate]
}

struct ResultTopRate: Codable {
    let adult: Bool
    let backdropPath: String
    let genreIDS: [Int]
    let id: Int
    let originalTitle, overview: String
    let popularity: Double
    let posterPath, releaseDate, title: String
    let video: Bool
    let voteAverage: Double
    let voteCount: Int

    enum CodingKeys: String, CodingKey {
        case adult
        case backdropPath = "backdrop_path"
        case genreIDS = "genre_ids"
        case id
        case originalTitle = "original_title"
        case overview, popularity
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case title, video
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
    }
}
