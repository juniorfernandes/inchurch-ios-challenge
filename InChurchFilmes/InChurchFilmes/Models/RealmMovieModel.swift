//
//  RealmMovieModel.swift
//  InChurchFilmes
//
//  Created by Junior Fernandes on 05/02/21.
//

import Foundation
import RealmSwift

final class FavoriteMovie: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var title: String = ""
    @objc dynamic var photo: String = ""
    @objc dynamic var releaseDate: String = ""
    @objc dynamic var overView: String = ""

    func setup(id: Int, title: String, photo: String, releaseDate: String, overView: String) {
        self.id = id
        self.title = title
        self.photo = photo
        self.releaseDate = releaseDate
        self.overView = overView
    }
}
