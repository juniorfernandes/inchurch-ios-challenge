//
//  Network.swift
//  InChurchFilmes
//
//  Created by Junior Fernandes on 03/02/21.
//

import Foundation
import Alamofire

enum EndPoint: String {
    case movie = "movie/popular?"
    case topRated = "movie/top_rated?"
    case detailMovie = "movie/"
    case genre = "genre/movie/list?"
    case search = "search/movie?"
}

class Network {
    static let shared = Network()

    func getPopularMovies(rota: String, page: Int = 1, completion: @escaping (Movies?, Error?) -> Void) {
        AF.request(rota).response { response in
            debugPrint(response)
            if response.response?.statusCode != 200 {
                print("ERROR")
                completion(nil, response.error)
            }
            do {
                let result = try JSONDecoder().decode(Movies.self, from: response.data!)
                if result != nil {
                    completion(result, nil)
                } else {
                    print("ERRORRRRRR========>")
                }
            } catch {
                print(error)
            }
        }
    }

    func getTopRatedMovies(rota: String, page: Int = 1, completion: @escaping (TopRatedMoviesModel?, Error?) -> Void) {
        AF.request(rota).response { response in
            debugPrint(response)
            if response.response?.statusCode != 200 {
                print("ERROR")
            }
//            guard response.error == nil else {
//                completion(nil, response.error)
//                return
//            }
            do {
                let result = try JSONDecoder().decode(TopRatedMoviesModel.self, from: response.data!)
                if result != nil {
                    completion(result, nil)
                } else {
                    print("ERRORRRRRR========>")
                }
            } catch {
                print(error)
            }
        }
    }

    func getGenrer(rota: String, completion: @escaping (ListGenre?, Error?) -> Void) {
        AF.request(rota).response { response in
            debugPrint(response)
            if response.response?.statusCode != 200 {
                print("ERROR")
                completion(nil, response.error)
                return
            }
            do {
                let result = try JSONDecoder().decode(ListGenre.self, from: response.data!)
                if result != nil {
                    completion(result, nil)
                    return
                } else {
                    print("ERRORRRRRRRR =========== GENTER")
                }
            } catch {
                print(error)
            }
        }
    }

    func searchMovie() {

    }
}
